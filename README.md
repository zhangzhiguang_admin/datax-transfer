
# 作者及联系方式
作者：莫多拿  
微信：moduona 
微信公众号： 薇豆芽福利社

## 公众号

分享各种编程语言、开发技术、分布式与微服务架构、分布式数据库、分布式事务、云原生、大数据与云计算技术和渗透技术。另外，还会分享各种面试题和面试技巧。内容在 **薇豆芽福利社** 微信公众号首发，强烈建议大家关注。

<div align="center">
    <img src="https://gitee.com/zhangzhiguang_admin/datax-transfer/blob/master/image/gongzhonghao.jpg" width="180px">
    <div style="font-size: 9px;">公众号：薇豆芽福利社</div>
    <br/>
</div>

# 薇豆芽福利社释放重磅福利

关注 **薇豆芽福利社** 微信公众号：

回复 “**我要进大厂**” 领取《我要进大厂系列之面试圣经》PDF电子书。

回复 “**java知识结构**” 领取《最新java知识面试宝典指南》PDF电子书。

回复 “**mysql精进**” 领取《高性能mysql》PDF电子书。


# datax-transfer

#### 介绍
分布式数据同步工具

# 使用步骤

## 运行系统

### 下载datax-transfer源码。

```bash
git clone https://gitee.com/zhangzhiguang_admin/datax-transfer.git
```

### 使用Maven编译datax-transfer

```bash
mvn clean package 
```

### 运行程序

```bash
java -jar datax-transfer.jar
```
