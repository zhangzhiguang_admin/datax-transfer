package com.blue.datax.mysqlSync.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping(value = "/xxljob")
public class xxlJobController{


    @GetMapping("jobTest")
    public void cachingTest(){
        log.info("------XXL-JOB 开始执行------");

        log.info("------XXL-JOB 结束------");

    }

}
