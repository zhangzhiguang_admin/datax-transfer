package com.blue.datax.mysqlSync.strategy;

import com.blue.datax.mysqlSync.annotation.TableCode;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class SyncDataStrategyDispatcher implements ApplicationContextAware {

    private static final String DELIMITER = "@";

    private ApplicationContext context;

    private final Map<String, AbstractHandleSyncData> instanceMap = new ConcurrentHashMap<>();

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }

    @PostConstruct
    public void register() {
        Map<String, AbstractHandleSyncData> appSolverMap = context.getBeansOfType(AbstractHandleSyncData.class);
        for (Map.Entry<String, AbstractHandleSyncData> strategyEntry : appSolverMap.entrySet()) {
            AbstractHandleSyncData value = strategyEntry.getValue();
            TableCode talbeCode = AnnotationUtils.findAnnotation(value.getClass(), TableCode.class);
            if (!ObjectUtils.isEmpty(talbeCode)) {
                instanceMap.put(talbeCode.value(), value);
            }
        }
    }


    public AbstractHandleSyncData getInstance(String talbeCode) {
        return instanceMap.get(talbeCode);
    }

}
