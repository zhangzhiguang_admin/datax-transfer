package com.blue.datax.mysqlSync.strategy;


import com.blue.datax.mysqlSync.entity.BinLogItem;
import lombok.extern.slf4j.Slf4j;

/**
 * 数据同步处理抽象类
 */
@Slf4j
public abstract  class AbstractHandleSyncData implements IHandleSyncData{

    public void handleData(BinLogItem item,String table){
        log.info("{},未匹配到表",table);
    }
}
