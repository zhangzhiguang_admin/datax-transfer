package com.blue.datax.mysqlSync.utils;

/**
 * @author zhishuo
 */
public enum DateEnum {
    DATE_FORMAT("yyyy-MM-dd HH:mm:ss"),
    DATE_SIMPLE("yyyy-MM-dd"),
    DATE_MIN("yy-MM-dd"),
    DATE_SIMPLE_MIN("yyyyMMdd"),
    DATE_YEAR("yyyy"),
    DATE_MONTH("MM"),
    DATE_DAY("dd"),
    DATE_CHINESE("yyyy年MM月dd日"),
    DATE_TIME_MIN("HHmmss"),
    DATE_TIME_HOURS("HH:mm:ss"),
    DATE_BANK_SEQ("yyyyMMddHHmmss"),
    DATE_BANK_SEQ_MILL("yyyyMMddHHmmssSSS"),
    DATE_HUABEI_SEQ_MILL_MIN("yyyyMMdd HHmmss"),
    DATE_HUABEI_SEQ_SMS("MM月dd日 HH:mm分"),
    DATE_BANK_SEQ_MILL_MIN("yyMMddHHmmssSSS"),
    DATE_SPLIT_SEQ("yyyy-MM-dd HH:mm:ss.sss"),
    DATE_SLASH("yyyy/MM/dd"),
    DATE_POINT("yyyy.MM.dd"),
    DATE_YYYYMM("yyyyMM"),
    DATE_YYYY_MM("yyyy-MM"),
    DATE_HHMMSSSSS("HHmmssSSS");
    private String text;

    private DateEnum(String text) {
        this.text = text;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
