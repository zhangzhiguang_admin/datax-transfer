package com.blue.datax.mysqlSync.utils;

import org.joda.time.*;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author zhishuo 时间计算工具类
 */
public final class DateUtils {
    /**
     * 获取当前时间
     *
     * @return 返回java Date
     * @author zhishuo
     */
    public static Date getNow() {
        return new DateTime().toDate();
    }

    /**
     * 获取当前时间
     *
     * @return 返回JODA DateTime
     * @author zhishuo
     */
    public static DateTime getDateTimeNow() {
        return new DateTime();
    }

    public static DateTime dateToDateTime(Date date) {
        return new DateTime(date);
    }

    /**
     * 字符串时间 转换为时间
     *
     * @author zhishuo
     */
    public static Date toDate(String date, DateEnum format) {
        DateTimeFormatter fmt = DateTimeFormat.forPattern(format.getText());
        return fmt.parseDateTime(date.trim()).toDate();
    }

    /**
     * 毫秒转换为Date
     *
     * @author zhishuo
     */
    public static Date msToDate(long ms) {
        return new DateTime(ms).toDate();
    }

    /**
     * 格式化时间
     *
     * @param date     时间
     * @param template 格式
     * @author zhishuo
     */
    public static String dateToStr(Date date, DateEnum template) {
        DateFormat format = new SimpleDateFormat(template.getText());
        return format.format(date);
    }

    /**
     * 传入日期增加几天
     *
     * @author zhishuo
     */
    public static Date plusDay(Date date, int day) {
        return new DateTime(date).plusDays(day).toDate();
    }

    /**
     * 传入日期增加几个月
     *
     * @author zhishuo
     */
    public static Date plusMonth(Date date, int month) {
        return new DateTime(date).plusMonths(month).toDate();
    }

    /**
     * 传入日期增加几个月
     *
     * @author zhishuo
     */
    public static Date plusMinutes(Date date, int minutes) {
        return new DateTime(date).plusMinutes(minutes).toDate();
    }


    /**
     * 传入日期减少几天
     *
     * @author zhishuo
     */
    public static Date minusDay(Date date, int day) {
        return new DateTime(date).minusDays(day).toDate();
    }


    /**
     * 传入日期减少月数
     */
    public static Date minusMonth(Date date, int month) {
        return new DateTime(date).minusMonths(month).toDate();
    }

    /**
     * 传入日期减少几分钟
     */
    public static Date minusMinutes(Date date, int minutes) {
        return new DateTime(date).minusMinutes(minutes).toDate();
    }

    /**
     * 传入日期减少几小时
     */
    public static Date minusHours(Date date, int hours) {
        return new DateTime(date).minusHours(hours).toDate();
    }

    public static Date longToDate(long millis) {
        return new DateTime(millis).toDate();
    }

    /**
     * 返回传入两个date相差的天数 END 大于 start 忽略时分秒 例：start = 2015-06-05 10:40:30 end = 2015-06-06 10:40:20
     * 返回值 1
     */
    public static int daysBetween(Date start, Date end) {
        int days = Days.daysBetween(new LocalDate(start), new LocalDate(end)).getDays();
        return days;
    }

    /**
     * 返回两个日期之间 月数
     *
     * @author zhishuo
     */
    public static int monthsBetween(Date start, Date end) {
        return monthsBetween(new DateTime(start), new DateTime(end));
    }

    /**
     * 返回两个日期之间 月数
     *
     * @author zhishuo
     */
    public static int monthsBetween(DateTime start, DateTime end) {
        int m = Months.monthsBetween(start, end).getMonths();
        return m;
    }

    /**
     * 返回两个日期之间 分钟数
     *
     * @author zhishuo
     */
    public static int minutesBetween(DateTime start, DateTime end) {
        int m = Minutes.minutesBetween(start, end).getMinutes();
        return m;
    }

    /**
     * 返回当前日期距离N月之后之后的天数 <p> 例如：计算1月5日，距离2个月之后的3月5日 返回相差天数
     */
    public static int daysAfterMonths(Date date, int addmonths) {
        DateTime dateTime = new DateTime(date);
        DateTime dateTimeAfter = dateTime.plusMonths(addmonths);
        return daysBetween(date, dateTimeAfter.toDate());
    }

    /**
     * 返回传入日期+N月之后的日期
     */
    public static Date dateAfterMonths(Date date, int addmonths) {
        DateTime dateTime = new DateTime(date);
        DateTime dateTimeAfter = dateTime.plusMonths(addmonths);
        return dateTimeAfter.toDate();
    }

    public static Date handleDateMix(Date date) {
        String str = dateToStr(date, DateEnum.DATE_SIMPLE) + " 00:00:00";
        return toDate(str, DateEnum.DATE_FORMAT);
    }

    public static Date handleDateMax(Date date) {
        String str = dateToStr(date, DateEnum.DATE_SIMPLE) + " 23:59:59";
        return toDate(str, DateEnum.DATE_FORMAT);
    }

    /**
     * 传入日期和当前日期比较（参数会转换成年月日格式）
     *
     * @return 小于-1，等于0，大于1
     */
    public static int compareDateToNow(DateTime dt) {
        return dt.toLocalDate().compareTo(new DateTime().toLocalDate());
    }

    public static int compareDate(DateTime d1, DateTime d2) {
        return d1.toLocalDate().compareTo(d2.toLocalDate());
    }

    /**
     * 返回当前时间格式yyyyMMdd字符串
     */
    public static String getSimpleDate(DateEnum template) {
        return dateToStr(getNow(), template);
    }

    /**
     * 返回当前时间格式yyyyMMdd字符串
     */
    public static String getSimpleDate() {
        return dateToStr(getNow(), DateEnum.DATE_SIMPLE_MIN);
    }
    /**
     * 返回当前时间格式yyyy-MM-dd字符串
     */
    public static String getDateSimple() {
        return dateToStr(getNow(), DateEnum.DATE_SIMPLE);
    }

    /**
     * 返回当前时间格式yyyyMMdd字符串
     */
    public static String getFullDate() {
        return dateToStr(getNow(), DateEnum.DATE_FORMAT);
    }

    /**
     * 返回当前时间格式HHmmss字符串
     */
    public static String getMinTime() {
        return dateToStr(getNow(), DateEnum.DATE_TIME_MIN);
    }

    /**
     * @return 当前月第几天
     */
    public static int getDayOfMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_MONTH);
    }


    public static Date getDate(String str, DateEnum format)  {
        SimpleDateFormat sdf = new SimpleDateFormat(format.getText());
        try {
            return sdf.parse(str);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getDate() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(new Date());
    }

    public static boolean checkValidDate(String str, DateEnum dateEnum) {
        SimpleDateFormat format = new SimpleDateFormat(dateEnum.getText());
        format.setLenient(false);
        try {
            Date a = format.parse(str);
            System.out.println(a);
            return true;
        } catch (ParseException e) {
            //如果throw java.text.ParseException或者NullPointerException，就说明格式不对
            return false;
        }
    }

    public static String getStringDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat formatter2 = new SimpleDateFormat("HHmmss");
        String dateString = formatter1.format(currentTime) + "T" + formatter2.format(currentTime);
        return dateString;
    }

    public static Date getReplaceDate(String str) {
        String dateTime = str.replaceAll("T", " ");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd hhmmss");
        Date date = null;
        try {
            date = simpleDateFormat.parse(dateTime);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date;
    }

    public static Date getZeroTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        Date zero = calendar.getTime();
      /*  SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String str = sdf.format(zero);*/
        return zero;

    }

    /**
     * 判断一个Date对象的时间是否是今天
     *
     * @param date
     * @return
     */
    public static boolean isToday(Date date) {
        // 今日零点
        DateTime todayBegin = new DateTime().withMillisOfDay(0);
        // 今日结束毫秒
        DateTime todayEnd = new DateTime().withMillisOfDay(0).plusDays(1).minusMillis(1);
        return todayBegin.getMillis() <= date.getTime() && todayEnd.getMillis() >= date.getTime();
    }

    /**
     * 判断nowTime时间是否在[startTime, endTime]区间，注意时间格式要一致
     *
     * @param nowTime 当前时间
     * @param startTime 开始时间
     * @param endTime 结束时间
     * @return
     */
    public static boolean isEffectiveDate(Date nowTime, Date startTime, Date endTime) {
        if (nowTime.getTime() == startTime.getTime()
                || nowTime.getTime() == endTime.getTime()) {
            return true;
        }

        Calendar date = Calendar.getInstance();
        date.setTime(nowTime);

        Calendar begin = Calendar.getInstance();
        begin.setTime(startTime);

        Calendar end = Calendar.getInstance();
        end.setTime(endTime);

        return date.after(begin) && date.before(end);
    }

    public static final int getActualMaximum(Date date){
        Calendar cal= Calendar.getInstance();
        cal.setTime(date);
        int maxDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        return maxDay;
    }

    /**
     * 日期比较
     * @param firstTime
     * @param secondTime
     * @return
     */
    public static int compareDate(String firstTime,String secondTime){
        return firstTime.compareTo(secondTime);
    }

    public static void main(String[] args) throws ParseException {

        Date date = new Date();
        System.out.println(DateUtils.dateToStr(date, DateEnum.DATE_FORMAT) + (isToday(date) ? " 是" : " 不是") + "今天");

    }

}
