package com.blue.datax.mysqlSync.utils.httputil;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;

import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @Description: 客户端连接工具-单例http
 * @Author: maqi
 * @Date: 2023/5/5 11:12
 */
@Slf4j
public class HttpClientUtils {

    public static final CloseableHttpClient singletonClient;

    private static final HttpClientConfig HTTP_CLIENT_CONFIG;

    private static final String ENCODING = "UTF-8";

    private static final RequestConfig requestConfig;

    static{
        HTTP_CLIENT_CONFIG = new HttpClientConfig();
        requestConfig = RequestConfig.custom()
                .setSocketTimeout(HTTP_CLIENT_CONFIG.getSocketTimeout())
                .setConnectTimeout(HTTP_CLIENT_CONFIG.getConnectionTimeout())
                .setConnectionRequestTimeout(HTTP_CLIENT_CONFIG.getConnectionRequestTimeout()).build();

        PoolingHttpClientConnectionManager poolingmgr = new PoolingHttpClientConnectionManager();
        poolingmgr.setDefaultMaxPerRoute(HTTP_CLIENT_CONFIG.getMaxPreRoute());
        poolingmgr.setMaxTotal(HTTP_CLIENT_CONFIG.getMaxTotal());

        singletonClient = HttpClients.custom()
                .setConnectionManager(poolingmgr)
                .setDefaultRequestConfig(requestConfig)
                //清除过期的连接
                .evictExpiredConnections()
                //清楚闲置的连接
                .evictIdleConnections(HTTP_CLIENT_CONFIG.getConnectionTimeToLive(), TimeUnit.SECONDS)
                .build();
    }
    private HttpClientUtils(){

    }

    public static HttpRespBody doGet(String url){
        return doGet(url,null, null);
    }

    public static HttpRespBody doGet(String url, Map<String, Object> params, Map<String, String> headers){
        log.info("get请求url:{},参数:{}", url, JSON.toJSONString(params));
        try {
            URIBuilder uriBuilder = new URIBuilder(url);
            if(null != params && !params.isEmpty()){
                Set<? extends Map.Entry<?, ?>> entrySet = params.entrySet();
                for(Map.Entry<?,?> entry : entrySet){
                    uriBuilder.addParameter((String) entry.getKey(),String.valueOf(entry.getValue()));
                }
            }
            HttpGet httpGet = new HttpGet(uriBuilder.build().toString());
            packageHeader(headers, httpGet);
            return execute(httpGet);
        } catch (URISyntaxException e) {
            log.error("http 请求构建连接失败", e);
        }
        return null;
    }

    public static HttpRespBody execute(HttpRequestBase request){
        long startTime = System.currentTimeMillis();
        try (CloseableHttpResponse response = singletonClient.execute(request)){
            long totalTimeMillis = System.currentTimeMillis() - startTime;
            log.info("请求time:{}", totalTimeMillis);
            return new HttpRespBody(response.getStatusLine().getStatusCode(), EntityUtils.toString(response.getEntity(), StandardCharsets.UTF_8));
        }catch (Exception e){
            log.error("http 请求发生异常", e);
        }
        return null;
    }

    public static void packageHeader(Map<String, String> headers, HttpRequestBase request){
        if(null != headers && !headers.isEmpty()){
            Set<Map.Entry<String, String>> entrySet = headers.entrySet();
            for(Map.Entry<String,String> entry : entrySet){
                request.setHeader(entry.getKey(), entry.getValue());
            }
        }
    }
}
