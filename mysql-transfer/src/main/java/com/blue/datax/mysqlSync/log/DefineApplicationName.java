package com.blue.datax.mysqlSync.log;

import ch.qos.logback.core.PropertyDefinerBase;


public class DefineApplicationName extends PropertyDefinerBase {
    @Override
    public String getPropertyValue() {
        return "calm-data-sync";
    }
}
