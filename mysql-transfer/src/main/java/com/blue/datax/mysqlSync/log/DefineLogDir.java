package com.blue.datax.mysqlSync.log;

import ch.qos.logback.core.PropertyDefinerBase;


public class DefineLogDir extends PropertyDefinerBase {
    @Override
    public String getPropertyValue() {
            return "/data/log/datax-transfer";
    }
}
