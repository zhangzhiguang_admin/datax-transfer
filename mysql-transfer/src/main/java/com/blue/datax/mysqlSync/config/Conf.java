package com.blue.datax.mysqlSync.config;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 数据库配置
 *
 **/
@Data
@AllArgsConstructor
public class Conf {
    private String host;
    private int port;
    private String username;
    private String passwd;
}
