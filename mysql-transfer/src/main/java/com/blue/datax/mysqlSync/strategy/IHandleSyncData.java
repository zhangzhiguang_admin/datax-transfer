package com.blue.datax.mysqlSync.strategy;

import com.blue.datax.mysqlSync.entity.BinLogItem;

/**
 * 数据同步处理类接口
 */
public interface IHandleSyncData {

    void handleData(BinLogItem item,String table);
}
