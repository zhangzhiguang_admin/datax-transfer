package com.blue.datax.mysqlSync.annotation;

import java.lang.annotation.*;

/**
 * @author zachary
 * @date 2024/3/12
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface TableCode {

    String value();
}
