package com.blue.datax.mysqlSync;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;
import com.blue.datax.mysqlSync.config.InitPropertiesListener;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class,scanBasePackages = {"com.blue.datax"})
@ServletComponentScan(value = {"com.blue.datax.mysqlSync"})
public class DataSyncApp {
    public static void main(String[] args) {

        try {
            SpringApplication application = new SpringApplication(DataSyncApp.class);
            application.addListeners(new InitPropertiesListener());
            application.run(args);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
