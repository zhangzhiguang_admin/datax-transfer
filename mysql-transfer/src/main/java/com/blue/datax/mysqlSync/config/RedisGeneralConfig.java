package com.blue.datax.mysqlSync.config;

import org.redisson.spring.data.connection.RedissonConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.io.Serializable;


@Configuration
public class RedisGeneralConfig {


    @Bean
    public RedisTemplate<Serializable, Object> redisTemplate(RedissonConnectionFactory redissonConnectionFactory) {
        return initRedisTemplate(redissonConnectionFactory);
    }


    protected RedisTemplate<Serializable, Object> initRedisTemplate(RedissonConnectionFactory redissonConnectionFactory){
        Jackson2JsonRedisSerializer<?> jackson2JsonRedisSerializer = new Jackson2JsonRedisSerializer<>(Object.class);
        jackson2JsonRedisSerializer.setObjectMapper(RedisJsonConfig.getMapper());
        RedisSerializer<?> stringSerializer = new StringRedisSerializer();
        RedisTemplate<Serializable, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(redissonConnectionFactory);
        template.setValueSerializer(jackson2JsonRedisSerializer);
        template.afterPropertiesSet();
        template.setKeySerializer(stringSerializer);
        template.setHashKeySerializer(stringSerializer);
        template.setHashValueSerializer(stringSerializer);

        return template;
    }



}
