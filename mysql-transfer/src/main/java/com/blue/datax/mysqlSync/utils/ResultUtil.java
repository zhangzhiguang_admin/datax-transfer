package com.blue.datax.mysqlSync.utils;


import com.blue.datax.mysqlSync.constant.ReturnCode;
import com.blue.datax.mysqlSync.dto.res.Result;

/**
 * Created by huyinglin on 2017/6/15.
 */
public class ResultUtil {

    public static Result success(Object object){
        Result result=new Result();
        result.setCode(ReturnCode.ACTIVE_SUCCESS.getCode());
        result.setMsg(ReturnCode.ACTIVE_SUCCESS.getMsg());
        result.setData(object);
        return result;
    }
    public static Result success(){
        return success(null);
    }
    public static  Result error(String code,String msg){
        Result result=new Result();
        result.setCode(code);
        result.setMsg(msg);
        return result;
    }

    public static Result noticeSuccess(Object object){
        Result result=new Result();
        result.setCode(ReturnCode.NOTICE_SUCCESS.getCode());
        result.setMsg(ReturnCode.NOTICE_SUCCESS.getMsg());
        result.setData(object);
        return result;
    }


}
