package com.blue.datax.mysqlSync.strategy;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.blue.datax.mysqlSync.utils.BinLogUtils;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.blue.datax.mysqlSync.annotation.TableCode;
import com.blue.datax.mysqlSync.entity.BinLogItem;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RLock;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
@TableCode(value = "bank_user_card_record")
public class BankUserCardRecordTable extends AbstractHandleSyncData{


    @Resource
    RedissonClient redissonClient;

    @Value(value = "${sync.data.update.colums:0}")
    private Set<String> updateColums;

    @Value(value = "${binlog.isopen:0}")
    private String isopen;

    public void handleData(BinLogItem item,String table){
        log.info("监听逻辑处理开始表{}",table);
        if("0".equals(isopen)){
            log.info("表{}变化，开关未打卡,不进行处理",table);
            return;
        }
        switch (BinLogUtils.getOptType(item)){
            case 1:
                addTableData(item);
                break;
            case 2:
                // 更新流程需要判断特定字段  customer_name ct_phone_number is_default protocol_number effective
                Boolean isUpdate = false;
                Set<String> updateCol = diffDBColums(item);
                if(CollectionUtil.isNotEmpty(updateCol)){
                    for(String col : updateColums){
                        if(updateCol.contains(col)){
                            isUpdate = true;
                            break;
                        }
                    }
                }

                if(isUpdate){
                    log.info("表{}变化，进行处理更新",table);
                    updateTableData(item);
                } else{
                    log.info("表{}变化，但不存在可更新字段,不进行处理",table);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 处理新增数据
     * @param item
     */
    private void addTableData(BinLogItem item){
        Map<String, Serializable> objMap = BinLogUtils.getOptMap(item);
        String ctCardNo = (String)objMap.get(convert("ctCardNo"));
        // 根据用户加密卡号进行redisson锁校验
        String redisKey =  "PAY:SYNC:CHECK:" + ctCardNo;
        RLock lock = redissonClient.getLock(redisKey);
        try {
            if (lock.tryLock(10,30*1000, TimeUnit.MILLISECONDS)) {
                log.info("新增实体为"+JSON.toJSONString(objMap));
            }else {
                log.info("新增数据，未获取到redisson锁，不处理");
            }

        }catch (Exception e){
            log.error("新增数据发生异常：",e);
        }finally {
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }

    }

    /**
     * 处理更新数据
     * @param item
     */
    private void updateTableData(BinLogItem item){
        Map<String, Serializable> objMap = BinLogUtils.getOptMap(item);
        String ctCardNo = (String)objMap.get(convert("ctCardNo"));
        // 根据用户加密卡号进行redisson锁校验
        String redisKey =  "PAY:SYNC:CHECK:" + ctCardNo;
        RLock lock = redissonClient.getLock(redisKey);
        try {
            if (lock.tryLock(10,30*1000, TimeUnit.MILLISECONDS)) {

                log.info("更新实体为"+JSON.toJSONString(objMap));
            }else {
                log.info("更新数据，未获取到redisson锁，不处理");
            }

        }catch (Exception e){
            log.error("更新数据发生异常：",e);
        }finally {
            if (lock.isLocked() && lock.isHeldByCurrentThread()) {
                lock.unlock();
            }
        }

    }

    public Set<String> diffDBColums(BinLogItem item){

        MapDifference<String, Object> difference = Maps.difference(item.getBefore(), item.getAfter());

        Map<String, MapDifference.ValueDifference<Object>> entriesDiffering = difference.entriesDiffering();
        log.info("更新变化的字段集合===：" + entriesDiffering);
        return entriesDiffering.keySet();
    }

    public <T> T fromMap(Map<String, Serializable> map, Class<T> clazz) {
        T instance;
        try {
            instance = clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                field.setAccessible(true); // 使私有字段也可以访问
                if (map.containsKey(convert(field.getName()))) {
                    field.set(instance, map.get(convert(field.getName())));
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return instance;
    }

    public static String convert(String camelCaseName) {
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < camelCaseName.length(); i++) {
            char ch = camelCaseName.charAt(i);
            if (Character.isUpperCase(ch)) {
                result.append("_");
                result.append(Character.toLowerCase(ch));
            } else {
                result.append(ch);
            }
        }
        return result.toString();
    }

}
