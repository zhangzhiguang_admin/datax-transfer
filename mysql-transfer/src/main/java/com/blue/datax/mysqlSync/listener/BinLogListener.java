package com.blue.datax.mysqlSync.listener;

import com.blue.datax.mysqlSync.entity.BinLogItem;

/**
 * BinLogListener监听器
 *
 * @author
 * @since
 **/
@FunctionalInterface
public interface BinLogListener {

    void onEvent(BinLogItem item);
}