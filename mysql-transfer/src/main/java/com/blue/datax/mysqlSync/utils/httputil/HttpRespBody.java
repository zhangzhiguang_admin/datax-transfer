package com.blue.datax.mysqlSync.utils.httputil;

/**
 * Created by admin on 2017/6/23.
 */
public class HttpRespBody {

    private int statusCode;
    private String result;

    public HttpRespBody(){}
    public HttpRespBody(int statusCode, String result){
        this.statusCode = statusCode;
        this.result = result;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

}
