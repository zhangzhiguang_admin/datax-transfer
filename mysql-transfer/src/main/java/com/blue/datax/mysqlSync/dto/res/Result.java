package com.blue.datax.mysqlSync.dto.res;

import java.io.Serializable;

/**
 * Created by huyinglin on 2017/6/16.
 */
public class Result<T> implements Serializable {

    private String code;
    private String msg;
    private T data;

    public void setCode(String code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String getCode() {

        return code;
    }

    public String getMsg() {
        return msg;
    }

    public T getData() {
        return data;
    }
}

