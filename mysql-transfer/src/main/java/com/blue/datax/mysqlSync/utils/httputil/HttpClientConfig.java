package com.blue.datax.mysqlSync.utils.httputil;

import lombok.Data;

/**
 * @Description: 客户端配置
 * @Author: maqi
 * @Date: 2023/5/5 11:03
 */
@Data
public class HttpClientConfig {

    /**
     * 连接管理器请求连接超时
     * 默认值 -1 无限超时
     **/
    private int connectionRequestTimeout = 4000;

    /**
     * 连接超时
     * 默认值 -1 无限超时
     **/
    private int connectionTimeout = 5000;

    /**
     * 读取数据超时
     * 默认值 -1 无限超时
     **/
    private int socketTimeout = 6000;

    /**
     * 总并行连接最大数
     **/
    private int maxTotal = 70;

    /**
     * 客户端路由最大数
     **/
    private int maxPreRoute = 20;

    /**
     * 连接存活时长：秒
     **/
    private long connectionTimeToLive = 60;

    /**
     * 重试次数
     **/
    private int retryCount = 3;

    /**
     * 非幂等请求是否可以重试
     **/
    private boolean requestSentRetryEnabled = false;
}
