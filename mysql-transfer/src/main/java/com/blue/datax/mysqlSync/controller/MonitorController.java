package com.blue.datax.mysqlSync.controller;

import com.blue.datax.mysqlSync.utils.ResultUtil;
import com.blue.datax.mysqlSync.dto.res.Result;
import org.springframework.core.env.Environment;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.lang.management.ManagementFactory;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zachary
 * @version 1.0.0
 */
@RestController
public class MonitorController {
    @Resource private Environment environment;

    @GetMapping("/monitor")
    public Result<Object> monitor() {
        String env = environment.getProperty("spring.profiles.active");
        String projectName = environment.getProperty("spring.application.name");
        LocalDateTime startTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(ManagementFactory.getRuntimeMXBean().getStartTime()), ZoneId.systemDefault());

        Map<String,Object> data = new HashMap<>();
        data.put("env", env);
        data.put("projectName", projectName);
        data.put("startTime", startTime);

        return ResultUtil.success(data);
    }
}
