package com.blue.datax.mysqlSync.constant;

/**
 * @ClassName: ReturnCode
 * @author: liuming
 * @date: 2016年2月23日 下午2:16:35
 */
public enum ReturnCode {
    ACTIVE_SUCCESS("000000", "操作成功"),
    ;
    private String code;

    private String msg;

    ReturnCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {

        return code;
    }

    public String getMsg() {
        return msg;
    }
}
