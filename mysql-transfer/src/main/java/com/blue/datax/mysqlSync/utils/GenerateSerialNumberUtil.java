package com.blue.datax.mysqlSync.utils;

import com.alibaba.fastjson.JSON;
import com.blue.datax.mysqlSync.constant.PayConstants;
import com.blue.datax.mysqlSync.utils.httputil.HttpClientUtils;
import com.blue.datax.mysqlSync.utils.httputil.HttpRespBody;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.UUID;

@Component
public class GenerateSerialNumberUtil {

    private static Logger logger = LoggerFactory.getLogger(GenerateSerialNumberUtil.class);

    @Value(value = "${sync.data.leafUrl:0}")
    private String leafUrl;


    /**
     * 1:调用 leaf 生成唯一标识
     * 2:组装支付业务流水号规则
     *
     * @param leafkey
     * @return
     */
    public String getLeafGenSerialNumber(String leafkey,String payType,String genModel) {
        String serialNumber;
        try {
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            logger.info("Leaf项目请求参数{}", uuid);
            HttpRespBody body = HttpClientUtils.doGet(leafUrl + PayConstants.LEAFMODEL.get(genModel) + leafkey + "?uuid=" + uuid);
            logger.info("Leaf项目请求返回{}-{}", uuid, JSON.toJSONString(body));
            if (null == body || 200 != body.getStatusCode()) {
                throw new Exception();
            }
            serialNumber = body.getResult();
            return PayConstants.PAYTYPE.get(payType) + serialNumber;
        } catch (Exception e) {
            logger.info("leaf替换使用临时数字，日期+随机数");
            return DateUtils.dateToStr(new Date(), DateEnum.DATE_BANK_SEQ_MILL_MIN) + RandomStringUtils.randomNumeric(5);
        }
    }

    public String getLeafByBindCardSerialNumber() {
        return getLeafGenSerialNumber("bindCard", "bindCard", "snowflake");
    }
}
